﻿Albunnack is a database and webservice that allows Musicbrainz and Discogs artists and releases to be retrieved using a common api.

Albunack Website uses Albunack to provide an integrated view of Musicbrainz and Discogs releases and artists and makes it easier to import or link Discogs data to MusicBrainz

This repository shows an example of an artist page and the associated stylesheets and javascript, it can used as the basis of ui improvemets by web designers.