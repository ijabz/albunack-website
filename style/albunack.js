function toggleMe(btn,a)
{
    var e=document.getElementById(a);
    if(!e)return true;
		e.style.display = e.style.display=="none"
		?"table-row"
		:"none";
    btn.src = btn.src.indexOf("images/open.png")>-1
        ?btn.src.replace("images/open.png","images/close.png")
        :btn.src.replace("images/close.png","images/open.png");
    return true;
}

$ (function () {
   $(".class-my-accordion").accordion({
        active: false,
        collapsible: true,
        heightStyle: "content"
    });
});